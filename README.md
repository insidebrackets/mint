# mintpage

git config --global user.name ""
git config --global user.email ""
git branch -m master main
git remote add origin https://gitlab.com/ ... .git

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
